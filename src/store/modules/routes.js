export default {
  namespaced: true,
  state: {
    secondaryRoutes: []
  },

  mutations: {
    set_secondaryRoutes: (state, routes) =>　{
      state.secondaryRoutes = routes
    }
  },
};
