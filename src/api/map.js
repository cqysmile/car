import request from "@/utils/request";

export function getMap(params) {
  return request({
    url: "/gps/getAll",
    method: "get",
    params
  });
}
