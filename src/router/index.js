import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
    hasSecondaryMenu: true       是否有二级菜单的依据
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },

  {
    path: "/404",
    component: () => import("@/views/404"),
    hidden: true
  },

  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () => import("@/views/dashboard/index"),
        meta: { title: "Dashboard", icon: "dashboard" }
      }
    ],
    meta: { title: "首页" }
  },
  {
    path: "/paho-map",
    component: () => import("@/views/paho-map/index"),
    meta: { title: "数据推送" }
  },

  {
    path: "/monitor-center",
    component: Layout,
    redirect: "/monitor-center/index",
    name: "MonitorCenterContainer",
    meta: { title: "监控中心", icon: "el-icon-s-help" },
    children: [
      {
        path: "index",
        name: "MonitorCenter",
        component: () => import("@/views/monitor-center/index"),
        meta: { title: "监控中心", icon: "table" }
      }
    ]
  },

  {
    path: "/track-playback",
    component: Layout,
    redirect: "/track-playback/index",
    children: [
      {
        path: "index",
        name: "TrackPlayback",
        component: () => import("@/views/track-playback/index"),
        meta: { title: "轨迹回放", icon: "form" }
      }
    ],
    meta: { title: "轨迹回放" }
  },

  {
    path: "/data-report",
    component: Layout,
    redirect: "/data-report/alarm-record",
    name: "DataReport",
    meta: {
      title: "数据报表",
      icon: "nested",
      hasSecondaryMenu: true
    },
    children: [
      {
        path: "alarm-record",
        component: () => import("@/views/data-report/alarm-record/index"), // Parent router-view
        name: "AlarmRecord",
        meta: { title: "报警记录", icon: "el-icon-eleme" }
      },
      {
        path: "offline-statistics",
        component: () => import("@/views/data-report/offline-statistics/index"), // Parent router-view
        name: "AlarmRecord",
        meta: { title: "离线统计", icon: "el-icon-eleme" }
      }
    ]
  },

  {
    path: "/data-manager",
    component: Layout,
    redirect: "/data-manager/user",
    name: "DataManeger",
    meta: {
      title: "资料管理",
      icon: "nested",
      hasSecondaryMenu: true
    },
    children: [
      {
        path: "user",
        component: () => import("@/views/data-manager/user/index"), // Parent router-view
        name: "User",
        meta: { title: "用户管理", icon: "el-icon-eleme" }
      }
      // {
      //   path: 'menu2',
      //   component: () => import('@/views/nested/menu2/index'),
      //   name: 'Menu2',
      //   meta: { title: 'menu2', icon: 'el-icon-eleme' }
      // }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: "*", redirect: "/404", hidden: true }
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
