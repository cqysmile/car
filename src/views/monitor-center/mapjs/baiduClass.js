import Map from "./index";
import { getMap } from "@/api/map";
export default class baiduClass extends Map {
  constructor(opts) {
    super(opts);
  }

  init() {
    document.getElementById(this.$el).innerHTML = "";
    const mapConstructor = this.mapConstructor;
    this.map = new mapConstructor.Map(this.$el, {
      mapType: this.mapType
    });
    var point = new mapConstructor.Point(116.404, 39.915);
    this.map.centerAndZoom(point, 15);
    this.map.enableScrollWheelZoom(true);
    this.toggleTrafficStatus();

    this.InfoWindow = new BMap.InfoWindow(
      document.getElementById("BdInfoWindow").cloneNode(true),
      {
        width: 400, // 信息窗口宽度
        height: 190 // 信息窗口高度
      }
    );
  }

  //绘制查询区域
  paintPolygon() {
    this.map.disableDragging();
    var overlaycomplete = function(e) {
      overlays.push(e.overlay);
    };

    var drawingManager = new BMapLib.DrawingManager(this.map, {
      isOpen: true,
      rectangleOptions: {
        strokeColor: "#3ba1fb", //边线颜色。
        fillColor: "rgba(59, 161, 251, 0.5)", //填充颜色。当参数为空时，圆形将没有填充效果。
        strokeWeight: 3, //边线的宽度，以像素为单位。
        strokeOpacity: 0.8, //边线透明度，取值范围0 - 1。
        fillOpacity: 0.6, //填充的透明度，取值范围0 - 1。
        strokeStyle: "solid" //边线的样式，solid或dashed。
      }
    });

    drawingManager.setDrawingMode(BMAP_DRAWING_RECTANGLE);

    drawingManager.addEventListener("overlaycomplete", e => {
      // console.log(e.overlay.Ao);
      this.map.enableDragging();
      this.map.removeOverlay(e.overlay);
      drawingManager.close();
      let isCover = BMapLib.GeoUtils.isPointInPolygon(
        new BMap.Point(103.989825, 30.640199),
        e.overlay
      );
      this.vueInstance.areaSearchResult(
        this.startPoint,
        this.endPonit,
        isCover
      );
    });
  }

  //添加位置点maker
  addPositionMaker(point) {
    if (this.positionMaker) {
      this.map.removeOverlay(this.positionMaker);
    }
    let positionPoint = new BMap.Point(point.lng, point.lat);
    this.positionMaker = new BMap.Marker(positionPoint);
    this.map.addOverlay(this.positionMaker);
    this.map.panTo(positionPoint);
  }

  //根据已有围栏数据画出区域
  addEnclosure(path) {
    let pathArr = [];
    let centerPoint = null;
    path.forEach((item, index) => {
      let point = new BMap.Point(item.lng, item.lat);
      index === 0 ? (centerPoint = point) : "";
      pathArr.push(point);
    });
    this.map.panTo(centerPoint);
    this.map.setZoom(13);
    this.EnclosureOverlay = new BMap.Polygon(pathArr);
    this.map.addOverlay(this.EnclosureOverlay);
  }

  //绘制二押点
  addSecond(point, region) {
    if (this.positionMaker) {
      this.map.removeOverlay(this.positionMaker);
    }
    let positionPoint = new BMap.Point(point.lng, point.lat);
    this.positionMaker = new BMap.Marker(positionPoint);
    this.map.addOverlay(this.positionMaker);

    this.secondArea = new BMap.Circle(positionPoint, Number(region));
    this.map.addOverlay(this.secondArea);
    this.map.panTo(positionPoint);
  }

  paintPolyline(pathArr, opts = {}) {
    let polyline = new BMap.Polyline([], opts);
    polyline.setPath(pathArr);
    this.map.addOverlay(polyline);

    return polyline;
  }

  //路线移动
  async runLuShu(params) {
    var lineArr = [];
    if (this.luShuOverlay) {
      // this.map.removeOverlay(this.luShuOverlay)
      this.map.clearOverlays();
    }
    // await getMap(params).then(res => {
    //   lineArr = res.data;
    // });
    await getMap(params).then(res => {
      lineArr = res.data.data.records;
    });

    // let lineArr = [
    // {time:1628208024,lng:116.478935,lat: 39.997761},
    // {time:1628211624,lng:116.478939,lat: 39.997825},
    // {time:1628215224,lng:116.478912,lat: 39.998549},
    // {time:1628218824,lng:116.478912,lat: 39.998549},
    // {time:1628219424,lng:116.478998,lat: 39.998555},
    // {time:1628223024,lng:116.478998,lat: 39.998555},
    // {time:1628226624,lng:116.479282,lat: 39.99856},
    // {time:1628230224,lng:116.479658,lat: 39.998528},
    // {time:1628233824,lng:116.480151,lat: 39.998453},
    // {time:1628237424,lng:116.480784,lat: 39.998302},
    // {time:1628241024,lng:116.480784,lat: 39.998302},
    // {time:1628244624,lng:116.481149,lat: 39.998184},
    // {time:1628248224,lng:116.481573,lat: 39.997997},
    // {time:1628251824,lng:116.481863,lat: 39.997846},
    // {time:1628255424,lng:116.482072,lat: 39.997718},
    // {time:1628259024,lng:116.482362,lat: 39.997718},
    // {time:1628269824,lng:116.483633,lat: 39.998935},
    // {time:1628273424,lng:116.48367, lat:39.998968},
    // {time:1628277024,lng:116.484648,lat: 39.999861}
    // ];

    let arr = lineArr.sort((a, b) => {
      return a.uploadDatetime - b.uploadDatetime;
    });

    let transformResult = [];
    arr = lineArr.map(item => {
      transformResult = this.GaodeLngLatToBaidu(item.longitude, item.latitude);
      return [transformResult.lng, transformResult.lat];

      // let transformResult = this.GaodeLngLatToBaidu(item[0], item[1]);
      // // console.log(transformResult)
      // return [transformResult.lng, transformResult.lat];
    });

    let pathArr = [];

    arr.forEach(item => {
      pathArr.push(new BMap.Point(item[0], item[1]));
    });

    //绘制轨迹路线
    // this.paintPolyline(pathArr, {
    //   strokeColor: "#90c3ff"
    // });

    let passArr = [];

    //绘制已行走路线
    let passedPolyline = this.paintPolyline([], {
      strokeColor: "#0b4c8f"
    });

    let center = new BMap.Point(arr[0][0], arr[0][1]);
    this.map.centerAndZoom(center, 15);

    let icon = new BMap.Icon(
      require("@/assets/map-image/offlineCar.png"),
      new BMap.Size(48, 48),
      {
        rotation: 90,
        imageOffset: new BMap.Size(0, 13)
      }
    );
    let LuShu = new BMapLib.LuShu(this.map, pathArr, {
      defaultContent: "轨迹回放",
      landmarkPois: [],
      icon: icon,
      enableRotation: true,
      speed: 3000
    });

    // setInterval(() => {
    //   console.log(JSON.parse(JSON.stringify(LuShu._marker.point)), 'LuShu')
    // }, 60)

    LuShu.start();
    let markerPonit = LuShu._marker.point;
    passArr.push(new BMap.Point(markerPonit.lng, markerPonit.lat));
    let endLng = arr[arr.length - 1][0];
    let endLat = arr[arr.length - 1][1];
    this.LuShutimer = setInterval(() => {
      passArr.push(
        new BMap.Point(LuShu._marker.point.lng, LuShu._marker.point.lat)
      );
      passedPolyline.setPath(passArr);

      // console.log(
      //   LuShu._marker.point.lng == endLng.toFixed(6) &&
      //     LuShu._marker.point.lat == endLat.toFixed(5)
      // );

      if (
        LuShu._marker.point.lng == endLng.toFixed(6) &&
        LuShu._marker.point.lat == endLat.toFixed(5)
      ) {
        clearInterval(this.LuShutimer);
      }
    }, 100);

    this.luShuOverlay = passedPolyline;
    this.liShuIcon = icon;
  }

  clearLuShuTimer() {
    clearInterval(this.LuShutimer);
  }

  //添加车信息和mark
  addCarMark(data, index, Bounds) {
    // let point = data.point;

    let position = new BMap.Point(data.point.lng,data.point.lat);
    // index === 0? this.map.panTo(position) : ''

    // let Bounds = this.map.getBounds();
    if (!Bounds.containsPoint(position)) {
      return;
    }

    let icon = new BMap.Icon(
      require("@/assets/map-image/offlineCar.png"),
      new BMap.Size(52, 26)
    );
    let car = new BMap.Marker(position, {
      icon,
      enableClicking: true
      // enableDragging: true
    });

    car.tag = index;

    // index === 0? this.map.panTo(position) : ''
    car.setZIndex(1000000);

    car.addEventListener("click", e => {
      // alert('click')
      console.log(e)
      if (this.isShowInfoCar && this.isShowInfoCar !== e.target) {
        this.isShowInfoCar.closeInfoWindow();
      }
      // console.log(e)
      this.vueInstance.setCarInfoWindow(data);


      //vue事件队列，通过promise保证dom更新，可用vue.$nextick替代
      Promise.resolve().then(() => {
        this.InfoWindow.setContent(document.getElementById("BdInfoWindow").cloneNode(true))
        e.target.openInfoWindow(this.InfoWindow);
        this.isShowInfoCar = e.target;
      });
    });

    window.requestAnimationFrame(() => {
      this.map.addOverlay(car);
    });

    // car.openInfoWindow(InfoWindow);
  }

  //打开全景
  openPanorama() {
    let Panorama = new BMap.Panorama("Panorama");
    Panorama.setPosition(new BMap.Point(120.31, 31.58));
    // Panorama.setPosition(new BMap.Point(116.404, 39.915))
    // Panorama.show()
  }
}

// let bc = new baiduClass({})
// export var runLuShu = bc.runLuShu
