import Map from "./index";

export default class gaodeClass extends Map {
  constructor(opts) {
    super(opts);
  }

  init() {
    document.getElementById(this.$el).innerHTML = "";
    const mapConstructor = this.mapConstructor;
    this.map = new mapConstructor.Map(this.$el, {
      resizeEnable: true,
      center: [116.404, 39.915],
      zoom: 13,
      layers: [this.mapType]
    });
    this.toggleTrafficStatus();
  }

  paintPolygon() {
    this.map.plugin(["AMap.MouseTool"], () => {
      var mousetool = new AMap.MouseTool(this.map);
      // 使用鼠标工具，在地图上画标记点
      mousetool.rectangle();
      mousetool.on("draw", e => {
        console.log(e.obj.aE); //经纬度
        // mousetool.close(true)
        let isCover = e.obj.contains(new AMap.LngLat(103.989825, 30.640199))
        this.map.setStatus({ dragEnable: true });
        this.vueInstance.areaSearchResult(this.startPoint, this.endPonit, isCover);
        mousetool.close(true)
      });
    });
  }

  //添加位置点maker
  addPositionMaker(point) {
    if (this.positionMaker) {
      this.map.remove(this.positionMaker);
    }
    let positionPoint = new AMap.LngLat(point.lng, point.lat);
    this.positionMaker = new AMap.Marker({
      position: positionPoint,
      anchor: "bottom-center"
    });
    this.map.add(this.positionMaker);
    // this.map.panTo(positionPoint);
    this.map.setCenter(positionPoint, true);
  }

  runLuShu() {
    let lineArr = [
      [116.478935, 39.997761],
      [116.478939, 39.997825],
      [116.478912, 39.998549],
      [116.478912, 39.998549],
      [116.478998, 39.998555],
      [116.478998, 39.998555],
      [116.479282, 39.99856],
      [116.479658, 39.998528],
      [116.480151, 39.998453],
      [116.480784, 39.998302],
      [116.480784, 39.998302],
      [116.481149, 39.998184],
      [116.481573, 39.997997],
      [116.481863, 39.997846],
      [116.482072, 39.997718],
      [116.482362, 39.997718],
      [116.483633, 39.998935],
      [116.48367, 39.998968],
      [116.484648, 39.999861]
    ];

    // let MoveAnimation = new AMap.MoveAnimation();

    // let centerPoint = new AMap.LngLat(116.478935, 39.997761)
    // this.map.setCenter(centerPoint)

    AMap.plugin(["AMap.MoveAnimation"], () => {
      let marker = new AMap.Marker({
        map: this.map,
        position: [116.478935, 39.997761],
        icon: "https://a.amap.com/jsapi_demos/static/demo-center-v2/car.png",
        offset: new AMap.Pixel(-13, -26)
        // autoRotation: true,
        // angle: -90
      });

      marker.on("moving", function(e) {
        passedPolyline.setPath(e.passedPath);
      });

      let pathArr = [];
      let time = 200;
      lineArr.forEach(item => {
        pathArr.push({
          position: new AMap.LngLat(item[0], item[1]),
          duration: time
        });

        time += 200;
      });

      // 绘制轨迹
      let polyline = new AMap.Polyline({
        map: this.map,
        path: lineArr,
        showDir: true,
        strokeColor: "#28F", //线颜色
        // strokeOpacity: 1,     //线透明度
        strokeWeight: 6 //线宽
        // strokeStyle: "solid"  //线样式
      });

      let passedPolyline = new AMap.Polyline({
        map: this.map,
        // path: lineArr,
        strokeColor: "#AF5", //线颜色
        // strokeOpacity: 1,     //线透明度
        strokeWeight: 6 //线宽
        // strokeStyle: "solid"  //线样式
      });

      // this.map.setCenter([116.397428, 39.90923]);
      this.map.setFitView();

      setTimeout(() => {
        marker.moveAlong(lineArr, {
          duration: 500,
          aniInterval: 0
          // JSAPI2.0 是否延道路自动设置角度在 moveAlong 里设置
          // autoRotation: true
        });
      }, 5000);
    });
  }

  //添加车信息和mark
  addCarMark(point) {
    point = this.BaiduLngLatToGaode(point.lng, point.lat);
    let position = new AMap.LngLat(point.lng, point.lat);
    let iconSize = new AMap.Size(52, 26);
    let icon = new AMap.Icon({
      size: iconSize,
      image: require("@/assets/map-image/offlineCar.png")
    });
    let car = new AMap.Marker({
      map: this.map,
      position: position,
      icon
    });

    let InfoWindow = new AMap.InfoWindow({
      content: document.getElementById("GdInfoWindow"),
      size: new AMap.Size(400, 190),
      offset: new AMap.Pixel(0, -30)
    });

    // this.map.panTo(position);

    this.map.add([car]);
    InfoWindow.open(this.map, position);
    InfoWindow.setAnchor("bottom-center");
    this.map.setCenter(position);

    // car.openInfoWindow(InfoWindow);
  }

  //根据已有围栏数据画出区域
  addEnclosure(path) {
    let pathArr = [];
    let centerPoint = null;
    path.forEach((item, index) => {
      let transPoint = this.BaiduLngLatToGaode(item.lng, item.lat)
      let point = new AMap.LngLat(transPoint.lng, transPoint.lat);
      index === 0 ? (centerPoint = point) : "";
      pathArr.push(point);
    });
    this.map.setCenter(centerPoint);
    this.map.setZoom(13);
    this.EnclosureOverlay = new AMap.Polygon({
      map: this.map,
      path: pathArr
    });
    this.map.add(this.EnclosureOverlay);
  }

  
  //绘制二押点
  addSecond(point, region) {
    // if (this.positionMaker) {
    //   this.map.remove(this.positionMaker);
    // }

    point = this.BaiduLngLatToGaode(point.lng, point.lat)
    let positionPoint = new AMap.LngLat(point.lng, point.lat);
    this.positionMaker = new AMap.Marker({
      map: this.map,
      position: positionPoint
    });
    this.map.add(this.positionMaker);

    this.secondArea = new AMap.Circle({
      map: this.map,
      center: positionPoint,
      radius: Number(region)
    });
    this.map.add(this.secondArea);
    this.map.setCenter(positionPoint);
    this.map.setZoom(18);
  }

  //打开全景
  openPanorama() {
    let Panorama = new BMap.Panorama('Panorama')
    Panorama.setPosition(new BMap.Point(120.31, 31.58));
    // Panorama.setPosition(new BMap.Point(116.404, 39.915))
    // Panorama.show()
  }
}
